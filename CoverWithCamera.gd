extends Control

export(NodePath) var target_layer
export(Texture) var basis_texture

func _process(_delta):
	var texsize = basis_texture.get_size()
	var scale = Vector2(
		rect_size.x / texsize.x,
		rect_size.y / texsize.y
	)
	var pos = rect_global_position + texsize*scale/2
	get_node(target_layer).transform = Transform2D(
		Vector2(scale.x, 0.0),
		Vector2(0.0, scale.y),
		pos
	)
