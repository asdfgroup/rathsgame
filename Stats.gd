extends VBoxContainer

func _process(_delta):
	var c = get_node("/root/Root/Critter")
	$StatBars/Stamina.value = c.current_stamina
	$StatBars/Stamina.max_value = c.max_stamina
	$StatBars/Happiness.value = c.current_happiness
	$StatBars/Happiness.max_value = c.max_happiness
	$StatBars/Hunger.value = c.current_hunger
	$StatBars/Hunger.max_value = c.max_hunger
	$StatBars/HP.value = c.current_hp
	$StatBars/HP.max_value = c.max_hp
	$StatBars/MP.value = c.current_mp
	$StatBars/MP.max_value = c.max_mp
	
	$StatNums/MaxStamina.text = String(c.max_stamina)
	$StatNums/MaxHP.text = String(c.max_hp)
	$StatNums/MaxMP.text = String(c.max_mp)
	$StatNums/Strength.text = String(c.strength)
	$StatNums/Intelligence.text = String(c.intelligence)
	$StatNums/Agility.text = String(c.agility)
	

