extends Button

export(String, FILE, "*.tscn") var scene

func _ready():
	var _shutup = connect("pressed", self, "_on_pressed")

func _on_pressed():
	var root = get_node("/root/Root");
	var old_screen = get_node("/root/Root/Screen")
	root.remove_child(old_screen)
	old_screen.queue_free()
	
	var sc = load(scene)
	var node = sc.instance()
	root.add_child(node)
	node.name = "Screen"
