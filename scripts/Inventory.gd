extends TabContainer

export(NodePath) var description
export(String) var placeholder_description = "Select an item"

onready var description_node = get_node(description)
var tree_item_to_item_node = Dictionary()

func _ready():
	description_node.text = placeholder_description
	
	recreate_tree()

func recreate_tree():
	for ch in get_children():
		remove_child(ch)
		ch.queue_free()
	tree_item_to_item_node = Dictionary()
	
	var critter = get_node("/root/Root/Critter")
	var items_node = get_node("/root/Root/Items")
	var categories = Dictionary()
	for item in items_node.get_children():
		var count = critter.inventory_amounts.get(item.name, 0)
		if count == 0:
			continue
		
		var category_tree
		if categories.has(item.category):
			category_tree = categories[item.category]
		else:
			category_tree = Tree.new()
			category_tree.columns = 2
			category_tree.hide_root = true
			category_tree.hide_folding = true
			category_tree.select_mode = Tree.SELECT_ROW
			category_tree.connect("item_selected", self, "_on_tree_selected", [category_tree])
			category_tree.create_item()
			add_child(category_tree)
			category_tree.name = item.category
			categories[item.category] = category_tree
		
		var tree_item = category_tree.create_item()
		tree_item.set_text(0, item.item_name)
		tree_item.set_text(1, "%s" % count)
		tree_item_to_item_node[tree_item] = item

func _on_tree_selected(tree):
	var selected = tree.get_selected()
	if !selected:
		description_node.text = placeholder_description
		return
	var item = tree_item_to_item_node[selected]
	description_node.text = item.description
