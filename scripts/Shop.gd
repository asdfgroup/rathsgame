extends TabContainer

export(NodePath) var description
export(String) var placeholder_description = "Buy something, won't ya?"

onready var description_node = get_node(description)
var tree_item_to_item_node = Dictionary()
var selected_item = null

func _ready():
	description_node.text = placeholder_description
	
	var items_node = get_node("/root/Root/Items")
	var categories = Dictionary()
	for item in items_node.get_children():
		var category_tree
		if categories.has(item.category):
			category_tree = categories[item.category]
		else:
			category_tree = Tree.new()
			category_tree.columns = 2
			category_tree.hide_root = true
			category_tree.hide_folding = true
			category_tree.select_mode = Tree.SELECT_ROW
			category_tree.connect("item_selected", self, "_on_tree_selected", [category_tree])
			category_tree.create_item()
			add_child(category_tree)
			category_tree.name = item.category
			categories[item.category] = category_tree
		
		var tree_item = category_tree.create_item()
		tree_item.set_text(0, item.item_name)
		tree_item.set_text(1, "$%s" % item.cost)
		tree_item_to_item_node[tree_item] = item

func _on_tree_selected(tree):
	var selected = tree.get_selected()
	if !selected:
		description_node.text = placeholder_description
		selected_item = null
		return
	var item = tree_item_to_item_node[selected]
	description_node.text = item.description
	selected_item = item.name


func _on_buy_pressed():
	if !selected_item:
		return
	var items_node = get_node("/root/Root/Items")
	var item_node = items_node.get_node(selected_item)
	var critter = get_node("/root/Root/Critter")
	
	if critter.take_money(item_node.cost):
		critter.inventory_amounts[item_node.name] = critter.inventory_amounts.get(item_node.name, 0) + 1
		description_node.text = "Thank you for your purchace!"
	else:
		description_node.text = "You don't have enough money!"
