extends "res://scripts/training_zones/training_zone_base.gd"

func is_visible(critter):
	return true

func activate(critter):
	var status = ""
	match randi() % 2:
		0:
			critter.max_hp += 2
			status += "HP increased by 2\n"
		1:
			critter.max_mp += 2
			status += "MP increased by 2\n"
	match randi() % 3:
		0:
			critter.strength += 1
			status += "Strength increased by 1\n"
		1:
			critter.agility += 1
			status += "Agility increased by 1\n"
		2:
			critter.intelligence += 1
			status += "Intelligence increased by 1\n"
	
	critter.money += 10
	status += "Gained $10\n"
	return status
