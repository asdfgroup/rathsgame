extends HBoxContainer

export(NodePath) var zone_name_label
export(NodePath) var zone_description_label
export(NodePath) var zone_image

var active_zone_name = null

func _ready():
	for ch in get_children():
		remove_child(ch)
	
	var zones_node = get_node("/root/Root/TrainingZones")
	var critter = get_node("/root/Root/Critter")
	for zone in zones_node.get_children():
		var button = Button.new()
		button.text = zone.zone_name
		button.name = zone.name
		button.visible = zone.is_visible(critter)
		button.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		button.connect("pressed", self, "_on_switch_area_button_pressed", [zone.name])
		add_child(button)
	_on_switch_area_button_pressed(zones_node.get_child(0).name)

func update_visibility():
	var zones_node = get_node("/root/Root/TrainingZones")
	var critter = get_node("/root/Root/Critter")
	for button in get_children():
		var zone = zones_node.get_node(button.name)
		button.visibility = zone.is_visible(critter)

func _on_switch_area_button_pressed(zone_node_name):
	var zones_node = get_node("/root/Root/TrainingZones")
	var zone_node = zones_node.get_node(zone_node_name)
	active_zone_name = zone_node_name
	get_node(zone_name_label).text = zone_node.zone_name
	get_node(zone_description_label).text = zone_node.description
	get_node(zone_image).texture = zone_node.icon

func _on_go_button_pressed():
	var zones_node = get_node("/root/Root/TrainingZones")
	var zone_node = zones_node.get_node(active_zone_name)
	var critter = get_node("/root/Root/Critter")
	var message = zone_node.activate(critter)
	
	var root = get_node("/root/Root");
	var old_screen = get_node("/root/Root/Screen")
	root.remove_child(old_screen)
	old_screen.queue_free()
	
	var results_screen = load("res://screens/training-results.tscn").instance()
	results_screen.find_node("TrainingAreaName").text = zone_node.zone_name
	results_screen.find_node("Results").text = message
	root.add_child(results_screen)
	results_screen.name = "Screen"
