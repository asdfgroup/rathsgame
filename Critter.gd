extends Node

export var age = 0

export var money = 20

export var max_happiness = 100
export var current_happiness = 50

export var max_stamina = 100
export var current_stamina = 100

export var max_hp = 100
export var current_hp = 100

export var max_mp = 100
export var current_mp = 100

export var max_hunger = 100
export var current_hunger = 0

export var strength = 10
export var intelligence = 10
export var agility = 10


export var inventory_amounts = {}

func change_happiness(delta):
	current_happiness = clamp(current_happiness + delta, 0, max_happiness)

func take_money(amnt):
	if money >= amnt:
		money -= amnt
		return true
	return false

func take_stamina(delta):
	if current_stamina >= delta:
		current_stamina -= delta
		return true
	return false

func take_mp(delta):
	if current_mp >= delta:
		current_mp -= delta
		return true
	return false
